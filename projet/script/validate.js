/*
* TODO.
* valider le formulaire
* valider si champs textes ont du contenu
* Valider les chaines de caracteres.
* valider le format du champ
* valider le textearea(valider si j'ai du contenu et si j'ai plus que 50 lettres dans mon contenu.)
* Valider mon courriel
* Populer la liste d'erreurs
*DOM = Document Object Model
*/
let error = [];
const form = document.getElementById("form");
const nom = document.getElementById("nom");
const prenom = document.getElementById("prenom");
const email = document.getElementById("courriel");
const comment = document.getElementById("comment");

const email_test = / ^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
const phone_test = / ^[0-9]{3}-[0-9]{3}-[0-9]{4}+$/;

const errorContainer = document.querySelector(".error__list");

const chaine_a_valider = [nom, prenom];
//console.log(form);

form.addEventListener("submit", function (e) {
    validateString();
    validateEmail();
    validateTextArea();
    e.preventDefault();
    //console.log(nom);
    //console.log(prenom);
});

function validateString(){
    for (let i = 0; i < chaine_a_valider.length; i++) {
        let chaine = chaine_a_valider[i];
        //console.log(chaine)
        //console.log(chaine.value.length)
        if (chaine.value.length <= 0) {
            error.push(`Le Champ ${chaine.id} dois etre rempli`);
            errorCaller(chaine);
        }
    }
}

function errorCaller(champ) {
    champ.classList.toggle("input__field--error");

}
function validateEmail() {
    if (email.value.length <= 0) {
        error.push(`Le Champ ${email.id} n'est pas rempli correctement`);
        errorCaller(email);
    }
    if (!email_test.test(email.value)) {
        error.push(`Le Champ ${email.id} n'est pas d'un format valide`);
        errorCaller(email);
    }
}
function validateTextArea() {
    //console.log(comment);
    //console.log(comment.value);
    let spaceless = comment.value.replace(/\s/g, '');
    if (spaceless == "" && spaceless >= 50) {
        errorCaller(comment);
    }
}

function errorContainerManager(){
    for (err in error){
        let tag = document.createElement("p");
        let error_text = document.createTextNode(error[err]);
        tag.appendChild(error_text);
        errorContainer.appendChild(tag);
        errorContainer.classList.add("error_list--visible");
    }
}